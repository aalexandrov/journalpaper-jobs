'''
Copyright 2010-2013 DIMA Research Group, TU Berlin

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
Created on Oct 14, 2011

@author: Alexander Alexandrov <alexander.alexandrov@tu-berlin.de>
'''

import logging.config
import optparse
import sys

class Generator(object):
    '''
    classdocs
    '''
    __basePath = None
    __fileName = None
    
    # a logger instance
    __log = None
    
    def __init__(self, basePath, fileName, argv):
        '''
        Constructor
        '''
        
        self.__basePath = basePath
        self.__fileName = fileName
        
        # initialize the logging subsystem
        # logging.config.fileConfig("%s/tools/config/assistant_logging.conf" % (self.__basePath))
        self.__log = logging.getLogger("generator")
        
    def run(self):
        '''
        Run the assistant
        '''
        N = 1000000
        try:
            for i in range(1, N-1):
                print "%d\t%d" % (i, i+1)
                print "%d\t%d" % (i+2, i)
            print "%d\t%d" % (N-1, N)
        except:
            e = sys.exc_info()[1]
            self.__printError("unexpected error: %s" % (str(e)), False)
            raise

