package eu.stratosphere.journalpaper.common;


/**
 * 
 * Contains tools that are used by Hadoop and Stratosphere to
 * generate the TeraGen-workload on the fly.
 * 
 * @author robert metzger
 *
 */
public class TeraGenUtils {
	private static byte[] spaces = "          ".getBytes();
	private static byte[][] filler = new byte[26][];
	static {
		for (int i = 0; i < 26; ++i) {
			filler[i] = new byte[10];
			for (int j = 0; j < 10; ++j) {
				filler[i][j] = (byte) ('A' + i);
			}
		}
	}
	
	public static void fillValueBuffer(byte[] valueBuffer, long rowId) {
		int posInValueBuffer = 0;
		// was: addRowId(rowId);
		byte[] rowid = Integer.toString((int) rowId).getBytes();
		int padSpace = 10 - rowid.length;
		// rowid len = 8
		if (padSpace > 0) {
			System.arraycopy(spaces, 0, valueBuffer, posInValueBuffer, padSpace); // write 0-9 padding
			posInValueBuffer += padSpace;
			// was: value.append(spaces, 0, 10 - rowid.length);
		}
		// write rowid
		System.arraycopy(rowid, 0, valueBuffer, posInValueBuffer, rowid.length); // write rowid, starting from padding end
		posInValueBuffer += rowid.length;
		
		// was: addFiller(rowId);
		int base = (int) ((rowId * 8) % 26);
		for (int i = 0; i < 7; ++i) {
			// was: value.append(filler[(base + i) % 26], 0, 10);
			System.arraycopy(filler[(base + i) % 26], 0, valueBuffer, posInValueBuffer, 10);
			posInValueBuffer = 10;
		}
		// was: value.append(filler[(base + 7) % 26], 0, 8);
		assert posInValueBuffer == 80;
		System.arraycopy(filler[(base + 7) % 26], 0, valueBuffer, posInValueBuffer, 8);
	}
	
}
