/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.stratosphere.journalpaper.terasort.hadoop.otf.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.InputSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RecordReader;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.util.IndexedSortable;
import org.apache.hadoop.util.QuickSort;

import eu.stratosphere.journalpaper.common.TeraGenUtils;

/**
 * An input format that reads the first 10 characters of each line as the key
 * and the rest of the line as the value. Both key and value are represented as
 * Text.
 */
@SuppressWarnings("deprecation")
public class TeraInputFormat extends FileInputFormat<Text, Text> {

	public static final String PARTITION_FILENAME = "_partition.lst";
	public static final String SAMPLE_SIZE = "terasort.partitions.sample";
	private static JobConf lastConf = null;
	private static InputSplit[] lastResult = null;

	static class TextSampler implements IndexedSortable {
		private ArrayList<Text> records = new ArrayList<Text>();

		@Override
		public int compare(int i, int j) {
			Text left = this.records.get(i);
			Text right = this.records.get(j);
			return left.compareTo(right);
		}

		@Override
		public void swap(int i, int j) {
			Text left = this.records.get(i);
			Text right = this.records.get(j);
			this.records.set(j, left);
			this.records.set(i, right);
		}

		public void addKey(Text key) {
			this.records.add(new Text(key));
		}

		/**
		 * Find the split points for a given sample. The sample keys are sorted
		 * and down sampled to find even split points for the partitions. The
		 * returned keys should be the start of their respective partitions.
		 * 
		 * @param numPartitions
		 *            the desired number of partitions
		 * @return an array of size numPartitions - 1 that holds the split
		 *         points
		 */
		Text[] createPartitions(int numPartitions) {
			int numRecords = this.records.size();
			System.out.println("Making " + numPartitions + " from "
					+ numRecords + " records");
			if (numPartitions > numRecords) {
				throw new IllegalArgumentException(
						"Requested more partitions than input keys ("
								+ numPartitions + " > " + numRecords + ")");
			}
			new QuickSort().sort(this, 0, this.records.size());
			float stepSize = numRecords / (float) numPartitions;
			System.out.println("Step size is " + stepSize);
			Text[] result = new Text[numPartitions - 1];
			for (int i = 1; i < numPartitions; ++i) {
				result[i - 1] = this.records.get(Math.round(stepSize * i));
			}
			return result;
		}
	}

	/**
	 * Use the input splits to take samples of the input and generate sample
	 * keys. By default reads 100,000 keys from 10 locations in the input, sorts
	 * them and picks N-1 keys to generate N equally sized partitions.
	 * 
	 * @param conf
	 *            the job to sample
	 * @param partFile
	 *            where to write the output file to
	 * @throws IOException
	 *             if something goes wrong
	 */
	public static void writePartitionFile(JobConf conf, Path partFile)
			throws IOException {
		TeraInputFormat inFormat = new TeraInputFormat();
		TextSampler sampler = new TextSampler();
		Text key = new Text();
		Text value = new Text();
		int partitions = conf.getNumReduceTasks();
		long sampleSize = conf.getLong(SAMPLE_SIZE, 100000);
		InputSplit[] splits = inFormat.getSplits(conf, conf.getNumMapTasks());
		int samples = Math.min(10, splits.length);
		long recordsPerSample = sampleSize / samples;
		int sampleStep = splits.length / samples;
		long records = 0;
		// take N samples from different parts of the input
		for (int i = 0; i < samples; ++i) {
			RecordReader<Text, Text> reader = inFormat.getRecordReader(
					splits[sampleStep * i], conf, null);
			while (reader.next(key, value)) {
				sampler.addKey(key);
				records += 1;
				if ((i + 1) * recordsPerSample <= records) {
					break;
				} 
			}
		}
		FileSystem outFs = partFile.getFileSystem(conf);
		if (outFs.exists(partFile)) {
			outFs.delete(partFile, false);
		}
		SequenceFile.Writer writer = SequenceFile.createWriter(outFs, conf,
				partFile, Text.class, NullWritable.class);
		NullWritable nullValue = NullWritable.get();
		for (Text split : sampler.createPartitions(partitions)) {
			writer.append(split, nullValue);
		}
		writer.close();
	}

	@Override
	public RecordReader<Text, Text> getRecordReader(InputSplit split,
			JobConf job, Reporter reporter) throws IOException {
		return new TeraRecordReader(job,  split);
	}

	/**
	 * TAKEN FROM TeraGen.java
	 * 
	 */
	static class TeraRecordReader implements RecordReader<Text, Text> {
		private long firstRow = -1;
		private long rowCount = -1;
		private long finishedRows = 0;
		
		private Text key = new Text();
		private byte[] valueBuffer = new byte[89];
		private Text value = new Text();
		private RandomGenerator rand;
		private byte[] keyBytes = new byte[12];
		private byte[] spaces = "          ".getBytes();
		private byte[][] filler = new byte[26][];
		{
			for (int i = 0; i < 26; ++i) {
				filler[i] = new byte[10];
				for (int j = 0; j < 10; ++j) {
					filler[i][j] = (byte) ('A' + i);
				}
			}
		}

		public TeraRecordReader(JobConf job, InputSplit split) {
			if(split instanceof RangeInputSplit) {
				firstRow = ((RangeInputSplit) split).firstRow;
				rowCount = ((RangeInputSplit) split).rowCount;
			} else {
				throw new RuntimeException("Unexpected and unsupported split type!");
			}
		}

		/**
		 * Add a random key to the text
		 * 
		 * @param rowId
		 */
		private void addKey() {
			for (int i = 0; i < 3; i++) {
				long temp = rand.next() / 52;
				keyBytes[3 + 4 * i] = (byte) (' ' + (temp % 95));
				temp /= 95;
				keyBytes[2 + 4 * i] = (byte) (' ' + (temp % 95));
				temp /= 95;
				keyBytes[1 + 4 * i] = (byte) (' ' + (temp % 95));
				temp /= 95;
				keyBytes[4 * i] = (byte) (' ' + (temp % 95));
			}
			key.set(keyBytes, 0, 10);
		}

		/**
		 * Add the rowid to the row.
		 * 
		 * @param rowId
		 */
//		private void addRowId(long rowId) {
//			byte[] rowid = Integer.toString((int) rowId).getBytes();
//			int padSpace = 10 - rowid.length;
//			if (padSpace > 0) {
//				value.append(spaces, 0, 10 - rowid.length);
//			}
//			value.append(rowid, 0, Math.min(rowid.length, 10));
//		}

		/**
		 * Add the required filler bytes. Each row consists of 7 blocks of 10
		 * characters and 1 block of 8 characters.
		 * 
		 * @param rowId
		 *            the current row number
		 */
//		private void addFiller(long rowId) {
//			int base = (int) ((rowId * 8) % 26);
//			for (int i = 0; i < 7; ++i) {
//				value.append(filler[(base + i) % 26], 0, 10);
//			}
//			value.append(filler[(base + 7) % 26], 0, 8);
//		}

		@Override
		public boolean next(Text key, Text value) throws IOException {
			this.key = key;
			long rowId = firstRow + finishedRows;
			if (rand == null) {
				// we use 3 random numbers per a row
				rand = new RandomGenerator(rowId * 3);
			}
			addKey();
			value.clear();
			//addRowId(rowId);
			//addFiller(rowId);
			TeraGenUtils.fillValueBuffer(valueBuffer, rowId);
			value.set(valueBuffer);
			if(finishedRows < rowCount) {
				finishedRows++;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public Text createKey() {
			return key;
		}

		@Override
		public Text createValue() {
			return value;
		}

		@Override
		public long getPos() throws IOException {
			return finishedRows; // I did not spend more time understanding this position ;)
		}

		@Override
		public void close() throws IOException {
			return;
		}

		@Override
		public float getProgress() throws IOException {
			return finishedRows/(float)rowCount;
		}
	}

	static class RandomGenerator {
		private long seed = 0;
		private static final long mask32 = (1l << 32) - 1;
		/**
		 * The number of iterations separating the precomputed seeds.
		 */
		private static final int seedSkip = 128 * 1024 * 1024;
		/**
		 * The precomputed seed values after every seedSkip iterations. There
		 * should be enough values so that a 2**32 iterations are covered.
		 */
		private static final long[] seeds = new long[] { 0L, 4160749568L,
				4026531840L, 3892314112L, 3758096384L, 3623878656L,
				3489660928L, 3355443200L, 3221225472L, 3087007744L,
				2952790016L, 2818572288L, 2684354560L, 2550136832L,
				2415919104L, 2281701376L, 2147483648L, 2013265920L,
				1879048192L, 1744830464L, 1610612736L, 1476395008L,
				1342177280L, 1207959552L, 1073741824L, 939524096L, 805306368L,
				671088640L, 536870912L, 402653184L, 268435456L, 134217728L, };

		/**
		 * Start the random number generator on the given iteration.
		 * 
		 * @param initalIteration
		 *            the iteration number to start on
		 */
		RandomGenerator(long initalIteration) {
			int baseIndex = (int) ((initalIteration & mask32) / seedSkip);
			seed = seeds[baseIndex];
			for (int i = 0; i < initalIteration % seedSkip; ++i) {
				next();
			}
		}

		RandomGenerator() {
			this(0);
		}

		long next() {
			seed = (seed * 3141592621l + 663896637) & mask32;
			return seed;
		}
	}

	/**
	 * An input split consisting of a range on numbers.
	 */
	static class RangeInputSplit implements InputSplit {
		long firstRow;
		long rowCount;

		public RangeInputSplit() {
		}

		public RangeInputSplit(long offset, long length) {
			firstRow = offset;
			rowCount = length;
		}

		public long getLength() throws IOException {
			return 0;
		}

		public String[] getLocations() throws IOException {
			return new String[] {};
		}

		public void readFields(DataInput in) throws IOException {
			firstRow = WritableUtils.readVLong(in);
			rowCount = WritableUtils.readVLong(in);
		}

		public void write(DataOutput out) throws IOException {
			WritableUtils.writeVLong(out, firstRow);
			WritableUtils.writeVLong(out, rowCount);
		}
	}

	static long getNumberOfRows(JobConf job) {
		return job.getLong("terasort.num-rows", 0);
	}

	

	/**
	 * 
	 * 
	 * Create the desired number of splits, dividing the number of rows between
	 * the mappers.
	 */
	public InputSplit[] getSplits(JobConf job, int numSplits) {
		if (job == lastConf) {
	      return lastResult;
	    }
		
		long totalRows = getNumberOfRows(job);
		long rowsPerSplit = totalRows / numSplits;
		System.out.println("Generating " + totalRows + " using " + numSplits
				+ " maps with step of " + rowsPerSplit);
		InputSplit[] splits = new InputSplit[numSplits];
		long currentRow = 0;
		for (int split = 0; split < numSplits - 1; ++split) {
			splits[split] = new RangeInputSplit(currentRow, rowsPerSplit);
			currentRow += rowsPerSplit;
		}
		splits[numSplits - 1] = new RangeInputSplit(currentRow, totalRows
				- currentRow);
		
		lastConf = job;
		lastResult = splits;
		return splits;
	}

}
