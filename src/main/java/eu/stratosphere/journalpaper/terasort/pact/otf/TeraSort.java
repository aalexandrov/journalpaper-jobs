/***********************************************************************************************************************
 *
 * Copyright (C) 2010 by the Stratosphere project (http://stratosphere.eu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 **********************************************************************************************************************/

package eu.stratosphere.journalpaper.terasort.pact.otf;


import eu.stratosphere.journalpaper.terasort.pact.otf.contract.OnTheFlyTerasortSource;
import eu.stratosphere.journalpaper.terasort.pact.otf.io.VoidOutputFormat;
import eu.stratosphere.pact.common.contract.GenericDataSink;
import eu.stratosphere.pact.common.contract.Order;
import eu.stratosphere.pact.common.contract.Ordering;
import eu.stratosphere.pact.common.plan.Plan;
import eu.stratosphere.pact.common.plan.PlanAssembler;
import eu.stratosphere.pact.common.plan.PlanAssemblerDescription;

/**
 * This is an example implementation of the famous TeraSort benchmark using the
 * Stratosphere system. The benchmark requires the input data to be generated
 * according to the rules of Jim Gray's sort benchmark. A possible way to such
 * input data is the Hadoop TeraGen program. For more details see <a href=
 * "http://hadoop.apache.org/common/docs/current/api/org/apache/hadoop/examples/terasort/TeraGen.html"
 * > http://hadoop.apache.org/common/docs/current/api/org/apache/hadoop/examples
 * /terasort/TeraGen.html</a>.
 * 
 * @author warneke
 */
public final class TeraSort implements PlanAssembler, PlanAssemblerDescription {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription() {
		return "Parameters: [noSubStasks] [noTeraSortRows] [output]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Plan getPlan(String... args) throws IllegalArgumentException {
		if (args.length < 3) {
			throw new IllegalArgumentException(this.getDescription());
		}

		final int noSubTasks = (args.length > 0 ? Integer.parseInt(args[0]) : 1);
		long teraTotalRows = (args.length > 1 ? Long.parseLong(args[1]) : 10000000L);
		final String output = (args.length > 2 ? args[2] : "");

		// This task writes the sorted data back to disk
		OnTheFlyTerasortSource source = new OnTheFlyTerasortSource( "Terasort generator");
		source.getParameters().setLong("teraTotalRows", teraTotalRows);

		GenericDataSink sink = new GenericDataSink(VoidOutputFormat.class, "Data Sink");
		sink.setGlobalOrder(new Ordering(0, TeraKey.class, Order.ASCENDING), new TeraDistribution());
		sink.addInput(source);

//		FileDataSink sink = new FileDataSink(TeraOutputFormat.class, output, "Data Sink");
//		sink.setGlobalOrder(new Ordering(0, TeraKey.class, Order.ASCENDING), new TeraDistribution());
//		sink.addInput(source);
		
		Plan p = new Plan(sink, "TeraSort");
		p.setDefaultParallelism(noSubTasks);
		return p;
	}

}
