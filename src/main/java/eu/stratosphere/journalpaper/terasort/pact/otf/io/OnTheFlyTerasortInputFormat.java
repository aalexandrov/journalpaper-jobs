/***********************************************************************************************************************
 *
 * Copyright (C) 2010 by the Stratosphere project (http://stratosphere.eu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 **********************************************************************************************************************/
package eu.stratosphere.journalpaper.terasort.pact.otf.io;

import java.io.IOException;

import eu.stratosphere.journalpaper.common.TeraGenUtils;
import eu.stratosphere.journalpaper.terasort.pact.otf.TeraKey;
import eu.stratosphere.journalpaper.terasort.pact.otf.TeraValue;
import eu.stratosphere.nephele.configuration.Configuration;
import eu.stratosphere.pact.common.generic.io.InputFormat;
import eu.stratosphere.pact.common.io.statistics.BaseStatistics;
import eu.stratosphere.pact.common.type.PactRecord;

/**
 * I assume that this class acts as a reader PER split!
 * @author robert
 */
public class OnTheFlyTerasortInputFormat implements InputFormat<PactRecord, OnTheFlyTerasortInputSplit> {

	private OnTheFlyTerasortInputSplit split;
	
	private TeraKey key = new TeraKey();
	private TeraValue value = new TeraValue();
	private byte[] valueBuffer = new byte[TeraValue.VALUE_SIZE];
	
	private RandomGenerator rand;
	private byte[] keyBytes = new byte[12];

	private long teraTotalRows = 10000000L;
	
	
	@Override
	public void configure(Configuration parameters) {
		this.teraTotalRows = parameters.getLong("teraTotalRows", 10000000L);
	}

	@Override
	public BaseStatistics getStatistics(BaseStatistics cachedStatistics) {
		return cachedStatistics; // no stats
	}

	@Override
	public OnTheFlyTerasortInputSplit[] createInputSplits(int minNumSplits)
			throws IOException {
		
		long rowsPerSplit = this.teraTotalRows  / minNumSplits;
		OnTheFlyTerasortInputSplit[] splits = new OnTheFlyTerasortInputSplit[minNumSplits];
		long currentRow = 0;
		for (int split = 0; split < minNumSplits - 1; ++split) {
			splits[split] = new OnTheFlyTerasortInputSplit(currentRow, rowsPerSplit, this.teraTotalRows);
			currentRow += rowsPerSplit;
		}
		splits[minNumSplits - 1] = new OnTheFlyTerasortInputSplit(currentRow, this.teraTotalRows - currentRow, this.teraTotalRows);
		return splits;
	}


	@Override
	public Class<? extends OnTheFlyTerasortInputSplit> getInputSplitType() {
		return OnTheFlyTerasortInputSplit.class;
	}

	@Override
	public void open(OnTheFlyTerasortInputSplit split) throws IOException {
		this.split = split;
		long rowId = split.firstRow + split.finishedRows;
		this.rand = new RandomGenerator(rowId * 3);
	}

	@Override
	public boolean reachedEnd() throws IOException {
		return this.split.finishedRows == this.split.rowCount;
	}

	@Override
	public boolean nextRecord(PactRecord record) throws IOException {
		assert this.rand != null;
		long rowId = this.split.firstRow + this.split.finishedRows;
		
		// was: addKey();
		for (int i = 0; i < 3; i++) {
			long temp = this.rand.next() / 52;
			this.keyBytes[3 + 4 * i] = (byte) (' ' + (temp % 95));
			temp /= 95;
			this.keyBytes[2 + 4 * i] = (byte) (' ' + (temp % 95));
			temp /= 95;
			this.keyBytes[1 + 4 * i] = (byte) (' ' + (temp % 95));
			temp /= 95;
			this.keyBytes[4 * i] = (byte) (' ' + (temp % 95));
		}
		
		// setting the key
		this.key.setValue(this.keyBytes, 0);
		record.setField(0, this.key);
		
		TeraGenUtils.fillValueBuffer(this.valueBuffer, rowId);
		
		// I'm not sure about the last byte. According to TeraValue, there are 89 bytes
		this.value.setValue(this.valueBuffer, 0);
		record.setField(1, this.value);
		
		this.split.finishedRows++;
		return true;
	}

	

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
	}

	static class RandomGenerator {
		private long seed = 0;
		private static final long mask32 = (1l << 32) - 1;
		/**
		 * The number of iterations separating the precomputed seeds.
		 */
		private static final int seedSkip = 128 * 1024 * 1024;
		/**
		 * The precomputed seed values after every seedSkip iterations. There
		 * should be enough values so that a 2**32 iterations are covered.
		 */
		private static final long[] seeds = new long[] { 0L, 4160749568L,
				4026531840L, 3892314112L, 3758096384L, 3623878656L,
				3489660928L, 3355443200L, 3221225472L, 3087007744L,
				2952790016L, 2818572288L, 2684354560L, 2550136832L,
				2415919104L, 2281701376L, 2147483648L, 2013265920L,
				1879048192L, 1744830464L, 1610612736L, 1476395008L,
				1342177280L, 1207959552L, 1073741824L, 939524096L, 805306368L,
				671088640L, 536870912L, 402653184L, 268435456L, 134217728L, };

		/**
		 * Start the random number generator on the given iteration.
		 * 
		 * @param initalIteration
		 *            the iteration number to start on
		 */
		RandomGenerator(long initalIteration) {
			int baseIndex = (int) ((initalIteration & mask32) / seedSkip);
			this.seed = seeds[baseIndex];
			for (int i = 0; i < initalIteration % seedSkip; ++i) {
				next();
			}
		}

		RandomGenerator() {
			this(0);
		}

		long next() {
			this.seed = (this.seed * 3141592621l + 663896637) & mask32;
			return this.seed;
		}
	}
}