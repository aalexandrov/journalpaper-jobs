/***********************************************************************************************************************
 *
 * Copyright (C) 2010 by the Stratosphere project (http://stratosphere.eu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 **********************************************************************************************************************/
package eu.stratosphere.journalpaper.terasort.pact.otf.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import eu.stratosphere.nephele.template.InputSplit;

/**
 * 
 * @author robert
 *
 */
public class OnTheFlyTerasortInputSplit implements InputSplit {

	long firstRow = -1;
	long rowCount = -1;
	long finishedRows = 0;
	long teraTotalRows = -1;

	// --------------------------------------------------------------------------------------------

	/**
	 * Default constructor for instantiation during de-serialization.
	 */
	public OnTheFlyTerasortInputSplit() {
	}
	
	public OnTheFlyTerasortInputSplit(long firstRow, long rowsPerSplit, long teraTotalRows) {
		this.firstRow = firstRow;
		this.rowCount = rowsPerSplit;
		this.teraTotalRows = teraTotalRows;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(this.firstRow);
		out.writeLong(this.rowCount);
		out.writeLong(this.finishedRows);
		out.writeLong(this.teraTotalRows);
	}

	@Override
	public void read(DataInput in) throws IOException {
		this.firstRow = in.readLong();
		this.rowCount = in.readLong();
		this.finishedRows = in.readLong();
		this.teraTotalRows = in.readLong();
	}

	@Override
	public int getSplitNumber() {
		return (int) (this.firstRow/this.teraTotalRows); 
	}
	
}