/***********************************************************************************************************************
 *
 * Copyright (C) 2010 by the Stratosphere project (http://stratosphere.eu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 **********************************************************************************************************************/
package eu.stratosphere.journalpaper.trienum.hadoop.io;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.InputSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.JobConfigurable;
import org.apache.hadoop.mapred.LineRecordReader;
import org.apache.hadoop.mapred.RecordReader;
import org.apache.hadoop.mapred.Reporter;

import eu.stratosphere.journalpaper.trienum.hadoop.io.type.IntTriple;

@SuppressWarnings("deprecation")
public class TriadInputFormat extends FileInputFormat<NullWritable, IntTriple> implements JobConfigurable {

	private CompressionCodecFactory compressionCodecs = null;

	@Override
	public void configure(JobConf conf) {
		this.compressionCodecs = new CompressionCodecFactory(conf);
	}

	@Override
	protected boolean isSplitable(FileSystem fs, Path file) {
		return this.compressionCodecs.getCodec(file) == null;
	}

	@Override
	public RecordReader<NullWritable, IntTriple> getRecordReader(InputSplit genericSplit, JobConf job, Reporter reporter)
			throws IOException {
		reporter.setStatus(genericSplit.toString());
		return new TriadRecordReader(job, (FileSplit) genericSplit);
	}

	public static class TriadRecordReader implements RecordReader<NullWritable, IntTriple> {

		public static final String ID_DELIMITER_CHAR = "triad.delimiter";

		private final LineRecordReader lineRecordReader;

		private LongWritable lineKey;

		private Text lineVal;

		private char delimiter;

		public Class<NullWritable> getKeyClass() {
			return NullWritable.class;
		}

		@Override
		public NullWritable createKey() {
			return NullWritable.get();
		}

		@Override
		public IntTriple createValue() {
			return new IntTriple();
		}

		public TriadRecordReader(Configuration job, FileSplit split) throws IOException {

			this.delimiter = (char) job.getInt(ID_DELIMITER_CHAR, '\t');
			this.lineRecordReader = new LineRecordReader(job, split);
			this.lineKey = this.lineRecordReader.createKey();
			this.lineVal = this.lineRecordReader.createValue();
		}

		@Override
		public synchronized boolean next(NullWritable key, IntTriple value) throws IOException {

			byte[] bytes = null;
			int numBytes = -1;
			int offset = 0;
			while (this.lineRecordReader.next(this.lineKey, this.lineVal)) {
				bytes = this.lineVal.getBytes();
				numBytes = this.lineVal.getLength();
				offset = 0;

				int first = 0, second = 0, third = 0;
				final char delimiter = this.delimiter;

				while (offset < numBytes && bytes[offset] != delimiter) {
					first = first * 10 + (bytes[offset++] - '0');
				}
				offset += 1;// skip the delimiter
				while (offset < numBytes && bytes[offset] != delimiter) {
					second = second * 10 + (bytes[offset++] - '0');
				}
				offset += 1;// skip the delimiter
				while (offset < numBytes) {
					third = third * 10 + (bytes[offset++] - '0');
				}

				if (first <= 0 || second <= 0 || second <= 0 || first == second || second == third || first == third)
					throw new RuntimeException("Invalid triple (" + first + "," + second + "," + third + ")");

				value.set(first, second, third);
				return true;

			}
			return false;
		}

		@Override
		public float getProgress() {
			return this.lineRecordReader.getProgress();
		}

		@Override
		public synchronized long getPos() throws IOException {
			return this.lineRecordReader.getPos();
		}

		@Override
		public synchronized void close() throws IOException {
			this.lineRecordReader.close();
		}
	}
}
