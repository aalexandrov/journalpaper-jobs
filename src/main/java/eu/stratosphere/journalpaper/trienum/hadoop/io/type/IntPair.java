package eu.stratosphere.journalpaper.trienum.hadoop.io.type;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * Define a pair of integers that are writable.
 * They are serialized in a byte comparable format.
 */
public class IntPair implements WritableComparable<IntPair> {

	private int first;

	private int second;

	public IntPair() {
		this.first = 0;
		this.second = 0;
	}

	public IntPair(int left, int right) {
		this.first = left;
		this.second = right;
	}

	/**
	 * Set the left and right values.
	 */
	public void set(int left, int right) {
		this.first = left;
		this.second = right;
	}

	public int getFirst() {
		return this.first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getSecond() {
		return this.second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	/**
	 * Read the two integers.
	 * Encoded as: MIN_VALUE -> 0, 0 -> -MIN_VALUE, MAX_VALUE-> -1
	 */
	@Override
	public void readFields(DataInput in) throws IOException {
		this.first = in.readInt() + Integer.MIN_VALUE;
		this.second = in.readInt() + Integer.MIN_VALUE;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(this.first - Integer.MIN_VALUE);
		out.writeInt(this.second - Integer.MIN_VALUE);
	}

	@Override
	public int hashCode() {
		return this.first * 157 + this.second;
	}

	@Override
	public boolean equals(Object right) {
		if (right instanceof IntPair) {
			IntPair r = (IntPair) right;
			return r.first == this.first && r.second == this.second;
		} else {
			return false;
		}
	}

	/** A Comparator that compares serialized IntPair. */
	public static class Comparator extends WritableComparator {
		public Comparator() {
			super(IntPair.class);
		}

		@Override
		public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
			return compareBytes(b1, s1, l1, b2, s2, l2);
		}
	}

	static { // register this comparator
		WritableComparator.define(IntPair.class, new Comparator());
	}

	@Override
	public int compareTo(IntPair o) {
		if (this.first != o.first) {
			return this.first < o.first ? -1 : 1;
		} else if (this.second != o.second) {
			return this.second < o.second ? -1 : 1;
		} else {
			return 0;
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.first);
		sb.append('\t');
		sb.append(this.second);
		return sb.toString();
	}
}