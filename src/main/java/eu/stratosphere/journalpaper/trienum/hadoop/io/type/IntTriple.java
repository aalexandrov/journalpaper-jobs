package eu.stratosphere.journalpaper.trienum.hadoop.io.type;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * Define a triple of integers that are writable.
 * They are serialized in a byte comparable format.
 */
public class IntTriple implements WritableComparable<IntTriple> {

	private int first;

	private int second;

	private int third;

	public IntTriple() {
		this.first = 0;
		this.second = 0;
		this.third = 0;
	}

	public IntTriple(int first, int second, int third) {
		this.first = first;
		this.second = second;
		this.third = third;
	}

	/**
	 * Set the left and right values.
	 */
	public void set(int first, int second, int third) {
		this.first = first;
		this.second = second;
		this.third = third;
	}

	public int getFirst() {
		return this.first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getSecond() {
		return this.second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public int getThird() {
		return this.third;
	}

	public void setThird(int third) {
		this.third = third;
	}

	/**
	 * Read the two integers.
	 * Encoded as: MIN_VALUE -> 0, 0 -> -MIN_VALUE, MAX_VALUE-> -1
	 */
	@Override
	public void readFields(DataInput in) throws IOException {
		this.first = in.readInt() + Integer.MIN_VALUE;
		this.second = in.readInt() + Integer.MIN_VALUE;
		this.third = in.readInt() + Integer.MIN_VALUE;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(this.first - Integer.MIN_VALUE);
		out.writeInt(this.second - Integer.MIN_VALUE);
		out.writeInt(this.third - Integer.MIN_VALUE);
	}

	@Override
	public int hashCode() {
		return (this.first * 2147483647 + this.second) * 2147483629 + this.third;
	}

	@Override
	public boolean equals(Object right) {
		if (right instanceof IntTriple) {
			IntTriple r = (IntTriple) right;
			return r.first == this.first && r.second == this.second && r.third == this.third;
		} else {
			return false;
		}
	}

	/** A Comparator that compares serialized IntTriple. */
	public static class Comparator extends WritableComparator {
		public Comparator() {
			super(IntTriple.class);
		}

		@Override
		public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
			return compareBytes(b1, s1, l1, b2, s2, l2);
		}
	}

	static { // register this comparator
		WritableComparator.define(IntTriple.class, new Comparator());
	}

	@Override
	public int compareTo(IntTriple o) {
		if (this.first != o.first) {
			return this.first < o.first ? -1 : 1;
		} else if (this.second != o.second) {
			return this.second < o.second ? -1 : 1;
		} else if (this.third != o.third) {
			return this.third < o.third ? -1 : 1;
		} else {
			return 0;
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.first);
		sb.append('\t');
		sb.append(this.second);
		sb.append('\t');
		sb.append(this.third);
		return sb.toString();
	}
}