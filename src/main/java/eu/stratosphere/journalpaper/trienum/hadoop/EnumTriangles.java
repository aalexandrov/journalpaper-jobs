/***********************************************************************************************************************
 *
 * Copyright (C) 2010 by the Stratosphere project (http://stratosphere.eu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 **********************************************************************************************************************/
package eu.stratosphere.journalpaper.trienum.hadoop;

import org.apache.hadoop.util.ToolRunner;

/**
 * @author alexander
 */
public class EnumTriangles {

	public static void main(String[] args) throws Exception {

		// check number of command line parameters
		if (args.length < 3) {
			throw new RuntimeException("Usage: trienum-job.jar [dop] [input] [output]");
		}

		String[] buildTriadsArgs = new String[3];
		buildTriadsArgs[0] = args[0]; // dop
		buildTriadsArgs[1] = args[1]; // edges path
		buildTriadsArgs[2] = args[2] + "/triads"; // triads path

		String[] closeTriadsArgs = new String[4];
		closeTriadsArgs[0] = args[0]; // dop
		closeTriadsArgs[1] = args[1]; // edges path
		closeTriadsArgs[2] = args[2] + "/triads";    // triads path
		closeTriadsArgs[3] = args[2] + "/triangles"; // output path

		int exitCode = -1;

		if ((exitCode = ToolRunner.run(new BuildTriads(), buildTriadsArgs)) != 0) { // run first job
			System.exit(exitCode);
		} else if ((exitCode = ToolRunner.run(new CloseTriads(), closeTriadsArgs)) != 0) { // run second job
			System.exit(exitCode);
		} else { // job complete, exit with 0
			System.exit(0);
		}
	}
}
