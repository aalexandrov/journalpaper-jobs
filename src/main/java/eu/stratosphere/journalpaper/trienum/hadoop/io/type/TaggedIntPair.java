/***********************************************************************************************************************
 *
 * Copyright (C) 2010 by the Stratosphere project (http://stratosphere.eu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 **********************************************************************************************************************/
package eu.stratosphere.journalpaper.trienum.hadoop.io.type;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class TaggedIntPair implements WritableComparable<TaggedIntPair> {

	private IntPair val;

	private int tag;

	public TaggedIntPair() {
		this.tag = 0;
		this.val = new IntPair();
	}

	public TaggedIntPair(int tag) {
		this.tag = tag;
		this.val = new IntPair();
	}

	public TaggedIntPair(int tag, IntPair val) {
		this.tag = tag;
		this.val = val;
	}

	public TaggedIntPair(int tag, int first, int second) {
		this.tag = tag;
		this.val = new IntPair(first, second);
	}

	public IntPair getVal() {
		return this.val;
	}

	public void setVal(int first, int second) {
		this.val.set(first, second);
	}

	public int getTag() {
		return this.tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.val.readFields(in);
		this.tag = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.val.write(out);
		out.writeInt(this.tag);
	}

	@Override
	public int compareTo(TaggedIntPair other) {
		int cmp = this.val.compareTo(other.getVal());
		if (cmp == 0) {
			return this.tag - other.getTag();
		} else {
			return cmp;
		}
	}

	@Override
	public int hashCode() {
		return this.val.hashCode() * 163 + (37 + this.tag);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof TaggedIntPair) {
			return this.compareTo((TaggedIntPair) o) == 0;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.val.toString() + " " + this.tag;
	}

}
