/***********************************************************************************************************************
 *
 * Copyright (C) 2010 by the Stratosphere project (http://stratosphere.eu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 **********************************************************************************************************************/
package eu.stratosphere.journalpaper.terasort.pact.otf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import eu.stratosphere.journalpaper.terasort.pact.otf.contract.OnTheFlyTerasortSource;
import eu.stratosphere.nephele.configuration.Configuration;
import eu.stratosphere.nephele.jobgraph.JobGraph;
import eu.stratosphere.pact.common.contract.FileDataSink;
import eu.stratosphere.pact.common.contract.Order;
import eu.stratosphere.pact.common.contract.Ordering;
import eu.stratosphere.pact.common.plan.Plan;
import eu.stratosphere.pact.compiler.PactCompiler;
import eu.stratosphere.pact.compiler.jobgen.JobGraphGenerator;
import eu.stratosphere.pact.compiler.plan.OptimizedPlan;
import eu.stratosphere.pact.test.util.TestBase;


/**
 * @author alexander
 */
@RunWith(Parameterized.class)
public class OnTheFlyInputFormatTest extends TestBase {
	
	/**
	 * Initializes OnTheFlyInputFormatTest.
	 *
	 * @param testConfig
	 */
	public OnTheFlyInputFormatTest(String clusterConfig, Configuration testConfig) {
		super(testConfig, clusterConfig);
	}

	/* (non-Javadoc)
	 * @see eu.stratosphere.pact.test.util.TestBase#getJobGraph()
	 */
	@Override
	protected JobGraph getJobGraph() throws Exception {

		final OnTheFlyTerasortSource source = new OnTheFlyTerasortSource( "Terasort on-the-fly Source");
		source.getParameters().setLong("teraTotalRows", 100000L);

		final FileDataSink sink = new FileDataSink(TeraOutputFormat.class,"file:///tmp/terasort.output.txt", "Data Sink");
		sink.setGlobalOrder(new Ordering(0, TeraKey.class, Order.ASCENDING), new TeraDistribution());
		sink.addInput(source);

		Plan plan = new Plan(sink, "TeraSort");
        OptimizedPlan op = new PactCompiler().compile(plan);
        return new JobGraphGenerator().compileJobGraph(op);
	}

	/* (non-Javadoc)
	 * @see eu.stratosphere.pact.test.util.TestBase#preSubmit()
	 */
	@Override
	protected void preSubmit() throws Exception {
	}

	/* (non-Javadoc)
	 * @see eu.stratosphere.pact.test.util.TestBase#postSubmit()
	 */
	@Override
	protected void postSubmit() throws Exception {
	}

	@Parameters
	public static Collection<Object[]> getConfigurations() throws FileNotFoundException, IOException {
		LinkedList<Configuration> testConfigs = new LinkedList<Configuration>();

		Configuration config = new Configuration();
		config.setInteger("MapTest#NoSubtasks", 4);
		testConfigs.add(config);

		return toParameterList(OnTheFlyInputFormatTest.class, testConfigs);
	}
}
