<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>eu.stratosphere</groupId>
  <artifactId>journalpaper-jobs</artifactId>
  <version>1.0.0</version>
  <packaging>jar</packaging>

  <name>journalpaper-jobs</name>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

  <repositories>
    <repository>
      <id>stratosphere.eu</id>
      <url>http://www.stratosphere.eu/maven2</url>
    </repository>
    <repository>
      <id>repo.maven.apache.org</id>
      <url>http://repo.maven.apache.org/maven2</url>
    </repository>
  </repositories>

  <dependencies>
    <!-- JUnit dependencies -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.7</version>
      <scope>test</scope>
    </dependency>
    <!-- Stratosphere dependencies -->
    <dependency>
      <groupId>eu.stratosphere</groupId>
      <artifactId>pact-common</artifactId>
      <version>0.2</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>eu.stratosphere</groupId>
      <artifactId>pact-tests</artifactId>
      <version>0.2</version>
      <type>test-jar</type>
      <scope>test</scope>
    </dependency>
    <!-- Hadoop dependencies -->
    <dependency>
      <groupId>org.apache.hadoop</groupId>
      <artifactId>hadoop-core</artifactId>
      <version>0.20.203.0</version>
      <scope>provided</scope>
    </dependency>
  </dependencies>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <configuration>
            <source>1.6</source>
            <target>1.6</target>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <!-- disable tests by default -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <skipTests>true</skipTests>
        </configuration>
      </plugin>
      <!-- packaging configuration -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>2.3.1</version>
        <executions>
          <!-- ***************************************************************** 
               * Utility Jobs 
               ************************************************************* -->
          
          <!-- word frequency extractor -->
          <execution>
            <id>util_wfe</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>util_wfe</classifier>
              <archive>
                <manifestEntries>
                  <Pact-Assembler-Class>eu.stratosphere.journalpaper.wordfrequency.pact.WordFrequencyExtractor</Pact-Assembler-Class>
                </manifestEntries>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/wordfrequency/pact/*.class</include>
              </includes>
            </configuration>
          </execution>
          
          <!-- graph edge duplicate removal -->
          <execution>
            <id>util_remdup</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>util_remdup</classifier>
              <archive>
                <manifestEntries>
                  <Pact-Assembler-Class>eu.stratosphere.journalpaper.trienum.pact.RemoveDuplicateEdges</Pact-Assembler-Class>
                </manifestEntries>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/trienum/pact/*.class</include>
                <include>**/eu/stratosphere/journalpaper/trienum/pact/**/*.class</include>
              </includes>
            </configuration>
          </execution>
          
          <!-- ***************************************************************** 
               * Experiment Jobs
               ************************************************************* -->
          
          <!-- word count (Stratosphere PACT) -->
          <execution>
            <id>wc-str_pact</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>wc-str_pact</classifier>
              <archive>
                <manifestEntries>
                  <Pact-Assembler-Class>eu.stratosphere.journalpaper.wordcount.pact.WordCount</Pact-Assembler-Class>
                </manifestEntries>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/wordcount/pact/WordCount*.class</include>
              </includes>
            </configuration>
          </execution>

          <!-- word count, Java StringTokenizer variant (Stratosphere PACT) -->
          <execution>
            <id>wc-jst-str_pact</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>wc-jst-str_pact</classifier>
              <archive>
                <manifestEntries>
                  <Pact-Assembler-Class>eu.stratosphere.journalpaper.wordcount.pact.WordCountWithJavaStringTokenizer</Pact-Assembler-Class>
                </manifestEntries>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/wordcount/pact/WordCountWithJavaStringTokenizer*.class</include>
              </includes>
            </configuration>
          </execution>

          <!-- word count (Hadoop MapReduce) -->
          <execution>
            <id>wc-hdp_mapr</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>wc-hdp_mapr</classifier>
              <archive>
                <manifest>
                  <mainClass>eu.stratosphere.journalpaper.wordcount.hadoop.WordCount</mainClass>
                </manifest>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/wordcount/hadoop/*.class</include>
                <include>**/eu/stratosphere/journalpaper/wordcount/hadoop/**/*.class</include>
              </includes>
            </configuration>
          </execution>

          <!-- tera sort (Stratosphere PACT) -->
          <execution>
            <id>ts_hdfs-str_pact</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>ts_hdfs-str_pact</classifier>
              <archive>
                <manifestEntries>
                  <Pact-Assembler-Class>eu.stratosphere.journalpaper.terasort.pact.vanilla.TeraSort</Pact-Assembler-Class>
                </manifestEntries>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/terasort/pact/vanilla/*.class</include>
                <include>**/eu/stratosphere/journalpaper/terasort/pact/vanilla/**/*.class</include>
              </includes>
            </configuration>
          </execution>

          <!-- tera sort (Hadoop MapReduce) -->
          <execution>
            <id>ts_hdfs-hdp_mapr</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>ts_hdfs-hdp_mapr</classifier>
              <archive>
                <manifest>
                  <mainClass>eu.stratosphere.journalpaper.terasort.hadoop.vanilla.TeraSort</mainClass>
                </manifest>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/terasort/hadoop/vanilla/*.class</include>
                <include>**/eu/stratosphere/journalpaper/terasort/hadoop/vanilla/**/*.class</include>
              </includes>
            </configuration>
          </execution>

          <!-- tera sort, on-the-fly source version (Hadoop MapReduce) -->
          <execution>
            <id>ts_otf-hdp_mapr</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>ts_otf-hdp_mapr</classifier>
              <archive>
                <manifest>
                  <mainClass>eu.stratosphere.journalpaper.terasort.hadoop.otf.TeraSort</mainClass>
                </manifest>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/terasort/hadoop/otf/*.class</include>
                <include>**/eu/stratosphere/journalpaper/terasort/hadoop/otf/**/*.class</include>
              </includes>
            </configuration>
          </execution>
          
         <execution>
            <id>ts_otf-str_pact</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>ts_otf-str_pact</classifier>
             <archive>
                <manifestEntries>
                  <Pact-Assembler-Class>eu.stratosphere.journalpaper.terasort.pact.otf.TeraSort</Pact-Assembler-Class>
                </manifestEntries>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/terasort/pact/otf/*.class</include>
                <include>**/eu/stratosphere/journalpaper/terasort/pact/otf/**/*.class</include>
              </includes>
            </configuration>
          </execution>

          <!-- TPCH Q3 (Stratosphere PACT) -->
          <execution>
            <id>tpch_Q3-str_pact</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>tpch_Q3-str_pact</classifier>
              <archive>
                <manifestEntries>
                  <Pact-Assembler-Class>eu.stratosphere.journalpaper.tpch.pact.TPCHQuery3</Pact-Assembler-Class>
                </manifestEntries>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/tpch/pact/*.class</include>
                <include>**/eu/stratosphere/journalpaper/tpch/pact/**/*.class</include>
              </includes>
            </configuration>
          </execution>

          <!-- triangle enumeration (Hadoop MapReduce) -->
          <execution>
            <id>te-hdp_mapr</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>te-hdp_mapr</classifier>
              <archive>
                <manifest>
                  <mainClass>eu.stratosphere.journalpaper.trienum.hadoop.EnumTriangles</mainClass>
                </manifest>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/trienum/hadoop/*.class</include>
                <include>**/eu/stratosphere/journalpaper/trienum/hadoop/**/*.class</include>
              </includes>
            </configuration>
          </execution>

          <!-- triangle enumeration (Stratosphere PACT) -->
          <execution>
            <id>te-str_pact</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
            <configuration>
              <classifier>te-str_pact</classifier>
              <archive>
                <manifestEntries>
                  <Pact-Assembler-Class>eu.stratosphere.journalpaper.trienum.pact.EnumTriangles</Pact-Assembler-Class>
                </manifestEntries>
              </archive>
              <includes>
                <include>**/eu/stratosphere/journalpaper/common/*.class</include>
                <include>**/eu/stratosphere/journalpaper/trienum/pact/*.class</include>
                <include>**/eu/stratosphere/journalpaper/trienum/pact/**/*.class</include>
              </includes>
            </configuration>
          </execution>

        </executions>
      </plugin>
    </plugins>
  </build>
</project>
